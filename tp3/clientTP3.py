# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP03 - peer-to-peer system with key-value storage

import utils
import sys
import socket
import struct

LOCAL_SEQ_NUM = 0
FLW_STATE = False

KEYREQ_CMD = "?"
TOPOREQ_CMD = "T"
FLW_CMD = "Q"

def print_menu():
    print("To search for a key: " + str(KEYREQ_CMD) + " DESIRED_KEY")
    print("To make a recognition of the topology, get a list all connected clients: " + str(TOPOREQ_CMD))
    print("To close the connection in this client: " + str(FLW_CMD))

""" MAIN PROGRAM BEGINS"""
try:
    if len(sys.argv) < 2:
        utils.debug('Wrong number of arguments! Must include "python3 clientTP3 <ip:port>"')
        sys.exit(0)

    ip_port = sys.argv[1].split(':')
    host = ip_port[0]
    port = int(ip_port[1])

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    print("Welcome to the peer-to-peer system!")
    print("\nThis is how you use this program:")
    print_menu()

    while True:

        # read command
        read_input = input()
        split_input = read_input.split()
        command = str(split_input[0])
        info = str(split_input[1:])

        # Build correct message
        if (command == TOPOREQ_CMD):
            msg = utils.build_message_by_type(utils.TOPOREQ_TYPE, LOCAL_SEQ_NUM)

        elif (command == KEYREQ_CMD):
            contents = ' '.join(split_input[1:])
            msg = utils.build_message_by_type(utils.KEYREQ_TYPE, LOCAL_SEQ_NUM, contents)

        elif (command == FLW_CMD):
            FLW_STATE = True
            utils.close_connection(client_socket)
            break
            # msg = utils.build_message_by_type(utils.FLW_TYPE, LOCAL_SEQ_NUM)

        else:
            print('Command unknown! "' + command + '" Please type a new valid command:')
            print_menu()
            continue


        trial = 2
        send = True
        received = False

        # Send message (retry once on timeout) and wait for response
        while trial > 0:
            try:
                if(send):
                    utils.send_data_udp(client_socket, host, port, msg)
                    send = False

                rcv_msg = utils.receive_message(client_socket, 4)

                if(rcv_msg == None):
                    if(received):
                        break

                    trial -= 1
                    send = True
                    continue

                if(rcv_msg[2] != utils.RESP_TYPE or rcv_msg[1] != LOCAL_SEQ_NUM):
                    print("Mensagem incorreta recebida de " + rcv_msg[0][0] + ":" + str(rcv_msg[0][1]))
                    utils.debug(str(rcv_msg[1]))
                    utils.debug(str(rcv_msg[2]))
                    utils.debug(str(LOCAL_SEQ_NUM))
                    continue

                # print received message
                print(rcv_msg[3] + " " + rcv_msg[0][0] + ":" + str(rcv_msg[0][1]))
                utils.debug(str(LOCAL_SEQ_NUM))
                received = True

            except socket.error as s_err:
                client_socket.close()
                print("Error - in client: "+ str(s_err))
                sys.exit(0)

        if(not received):
            print("Nenhuma resposta recebida")

        LOCAL_SEQ_NUM += 1

# terminates connection CTRL+C is typed
except KeyboardInterrupt as e:
    utils.close_connection(client_socket)
    utils.debug('The client socket connection is closed')



""" MAIN PROGRAM ENDS"""
