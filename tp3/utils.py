# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP03 - peer-to-peer system with key-value storage

import sys
import socket
import struct
import re

DEBUG = False

""" Messages Types """
KEYREQ_TYPE = 5
TOPOREQ_TYPE = 6
KEYFLOOD_TYPE = 7
TOPOFLOOD_TYPE = 8
RESP_TYPE = 9
""" Messages Types """

MAX_UDP_BUFFER = 4096

# Prints debug messages if DEBUG is set
def debug(message):
    if DEBUG:
        print('[debug] ' + message)

def build_message_by_type(msg_type, seq_num, key_info = "", ttl = 0, IP_ORIG = '', PORT_ORIG = 0):
    debug("msg_type - seq_num - ttl - IP_ORIG- PORT_ORIG:")
    debug(str(msg_type) + " - " + str(seq_num) + " - " + str(ttl) + " - " + str(IP_ORIG)+ " - " + str(PORT_ORIG))

    if msg_type == KEYREQ_TYPE or msg_type == RESP_TYPE:
        ascii_contents = key_info.encode('ascii')
        # contents_length = len(ascii_contents)
        msg = struct.pack("!H", msg_type) + struct.pack("!I", seq_num) + ascii_contents
    elif msg_type == TOPOREQ_TYPE:
        msg = struct.pack("!H", msg_type) + struct.pack("!I", seq_num)
    elif msg_type == KEYFLOOD_TYPE or msg_type == TOPOFLOOD_TYPE:
        ascii_contents = key_info.encode('ascii')
        ip_split = [int(x) for x in IP_ORIG.split('.')]
        ip_bin = struct.pack("!BBBB", ip_split[0], ip_split[1], ip_split[2], ip_split[3])
        msg = struct.pack("!H", msg_type)+ struct.pack("!H", ttl) + struct.pack("!I", seq_num) + ip_bin + struct.pack("!H", PORT_ORIG) + ascii_contents
    # else:
    #     ascii_contents = 'THERE WAS AN ERROR'.encode('ascii')
    #     msg = struct.pack("!H", RESP_TYPE) + struct.pack("!H", seq_num) + ascii_contents
    return msg

# Initialize UDP socket and send data
def send_data_udp(current_socket, host, port, message):
    try:
        # Send data
        debug('sending ' +str(message))
        debug('host ' +str(host)+' port '+str(port))
        current_socket.sendto(message, (host, port))

    except socket.error as s_err:
        current_socket.close()
        debug ('ERROR in send_data_udp: '+str(s_err))
        sys.exit(0)

def close_connection(socket):
    if socket is not None:
        debug('closing socket')
        socket.close()

def receive_data_udp(current_socket, size = 1, timeout = None):
    total_received = 0
    data = bytearray()

    try:
        # make sure all the data was received, even if sent in different packages
        current_socket.settimeout(timeout)
        data, address = current_socket.recvfrom(size)

        if(data == ''):
            raise RuntimeError('ERROR in receive_data - Connection to client lost')

        return data, address

    except socket.timeout as timeout:
        return None

    except socket.error as s_err:
        current_socket.close()
        print("Error - in receive_data_udp: "+ str(s_err))
        sys.exit(0)

# Reads receive and decode a message from the socket, as defined
# in the protocol
def receive_message(source_socket, timeout = None):
    address=None
    message_type=None
    seq_num=None
    key_info=""
    ttl=None
    ip_orig=None
    port_orig=None

    received = receive_data_udp(source_socket, MAX_UDP_BUFFER, timeout)

    if(received == None):
        return None

    package = received[0]
    address = received[1]

    message_type = clean_unpacked_input(struct.unpack("!H", package[0:2]))

    if(message_type == KEYREQ_TYPE or message_type == RESP_TYPE):
        seq_num = clean_unpacked_input(struct.unpack("!I", package[2:6]))

        key_info = clean_unpacked_input(package[6:].decode('ascii'),'str')

        return address, seq_num, message_type, key_info, ttl, ip_orig, port_orig

    if(message_type == TOPOREQ_TYPE):
        seq_num = clean_unpacked_input(struct.unpack("!I", package[2:6]))

        return address, seq_num, message_type, key_info, ttl, ip_orig, port_orig

    if(message_type == KEYFLOOD_TYPE or message_type == TOPOFLOOD_TYPE):
        ttl = clean_unpacked_input(struct.unpack("!H", package[2:4]), 'int')
        seq_num = clean_unpacked_input(struct.unpack("!I", package[4:8]), 'int')
        ip_orig = clean_unpacked_input(struct.unpack("!BBBB", package[8:12]),'byte')
        port_orig = clean_unpacked_input(struct.unpack("!H", package[12:14]), 'int')
        key_info = clean_unpacked_input(package[14:].decode('ascii'),'str')

        return address, seq_num, message_type, key_info, ttl, ip_orig, port_orig

def clean_unpacked_input(input_list, mode = 'int'):
    if mode is 'int':
        list_to_string = ''.join(str(v) for v in input_list)
        re.sub(r"\D", "", list_to_string)
        return int(list_to_string)
    elif mode is 'str':
        list_to_string = ''.join(input_list)
        # re.sub(r"\D", "", list_to_string)
        return list_to_string
    elif mode is 'byte':
        list_to_string = '.'.join(str(x) for x in input_list)
        return str(list_to_string)
