# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP03 - peer-to-peer system with key-value storage

import utils
import sys
import socket
import struct

TTL_VALUE = 3
LOCAL_IP = '127.0.0.1'

def create_key_values_list(filename):
    database = dict()
    with open(filename) as fin:
        for line in fin:
            if line.startswith("#"):
                utils.debug('Ignoring comment line: ' + line)
                pass
            elif line not in ['\n', '\r\n']:
                split_line = line.split()
                key = split_line[0]
                value = ' '.join(split_line[1:])
                database[key] = value

    if utils.DEBUG:
        print_dictionary(database)

    return database

def print_dictionary(dictionary):
    print('This is the dictionary:')
    for k, v in dictionary.items():
        print('KEY: ' + k +' - VALUE: '+v)


def check_if_key_is_present(dic, key, seq_num, current_socket, ip_orig, port_orig):
    if key in dic:
        utils.debug('Key was found! '+ str(dic[key]))
        msg = utils.build_message_by_type(utils.RESP_TYPE, seq_num, dic[key])
        utils.send_data_udp(current_socket, ip_orig, port_orig, msg)
        return True
    else:
        utils.debug('Key was NOT found! '+ str(key))
        return False

def check_if_message_in_history(history_dic, address, seq_num, ip_orig, port_orig):
    key = str(ip_orig+':'+str(port_orig)+':'+str(seq_num))
    if key in history_dic:
        utils.debug('Message is in history! '+ str(history_dic[key]))
        return True
    else:
        utils.debug('Message is NOT in history! '+ str(key))
        return False

def add_flood_message_to_history(history_dic, address, seq_num, ip_orig, port_orig, info):
    # Adds this message to dictionary, the format is the clients dic[IP:PORT:NSEQ] = INFO
    key = str(ip_orig+':'+str(port_orig)+':'+str(seq_num))
    value = str(info)
    history_dic[key] = value

    if utils.DEBUG:
        print_dictionary(history_dic)

def spread_flood_message(address, message_type, ttl, seq_num, ip_orig, port_orig, info):
    ip_port_orig = address[0]+":"+str(address[1])

    ttl -= 1
    if ttl > 0:
        msg = utils.build_message_by_type(message_type, seq_num, info, ttl, ip_orig, port_orig)
        send_to_neighbours_except_origin(servent_socket, servent_list, msg, ip_port_orig)

def send_to_neighbours_except_origin(current_socket, neighbours_list, message, origin_servent = None):
    utils.debug('The origin servent addr is:'+ str(origin_servent))
    for item in neighbours_list:
        if item != origin_servent:
            split_address = item.split(':')
            utils.send_data_udp(current_socket, split_address[0], int(split_address[1]), message)

""" MAIN PROGRAM BEGINS"""

if len(sys.argv) < 2:
    utils.debug('Wrong number of arguments! Must be "TP3node <porto-local> <banco-chave-valor> [ip1:porto1 [ip2:porto2 ...]]"')
    sys.exit(0)

port = int(sys.argv[1])
database_filename = sys.argv[2]

# Reads all servents <ip:port> pairs from command line, appending them to a list
servent_list = []
for item in sys.argv[3:]:
    if len(servent_list) == 10 or item is None:
        break
    else:
        no_brackets = str.maketrans(dict.fromkeys('[]'))
        item = item.translate(no_brackets)
        servent_list.append(item)

# From the given file in command line, creates a dictionary with <key, value> pairs
database = create_key_values_list(database_filename)

# Binds the UDP connection, listens for messages in the PORT given
servent_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
servent_socket.bind((LOCAL_IP, port))

messages_history = dict()

try:
    while True:
        address, seq_num, message_type, key_info, ttl, ip_orig, port_orig = utils.receive_message(servent_socket)
        utils.debug("Received command:" + str(message_type))

        if (message_type == utils.KEYREQ_TYPE):
            result = check_if_key_is_present(database, key_info, seq_num, servent_socket, address[0], address[1])

            msg = utils.build_message_by_type(utils.KEYFLOOD_TYPE, seq_num, key_info, TTL_VALUE, address[0], address[1])
            send_to_neighbours_except_origin(servent_socket, servent_list, msg)

        elif (message_type == utils.TOPOREQ_TYPE):
            current_address = servent_socket.getsockname()
            new_info = str(current_address[0])+':'+str(current_address[1])

            response = utils.build_message_by_type(utils.RESP_TYPE, seq_num, new_info)
            utils.send_data_udp(servent_socket, address[0], address[1], response)

            msg = utils.build_message_by_type(utils.TOPOFLOOD_TYPE, seq_num, new_info, TTL_VALUE, address[0], address[1])
            send_to_neighbours_except_origin(servent_socket, servent_list, msg, address)

        elif (message_type == utils.KEYFLOOD_TYPE):
            msg_history = check_if_message_in_history(messages_history, address, seq_num, ip_orig, port_orig)

            if msg_history:
                pass
            else:
                add_flood_message_to_history(messages_history, address, seq_num, ip_orig, port_orig, key_info)
                check_if_key_is_present(database, key_info, seq_num, servent_socket, ip_orig, port_orig)
                spread_flood_message(address, message_type, ttl, seq_num, ip_orig, port_orig, key_info)

        elif (message_type == utils.TOPOFLOOD_TYPE):
            msg_history = check_if_message_in_history(messages_history, address, seq_num, ip_orig, port_orig)

            if msg_history:
                pass
            else:
                add_flood_message_to_history(messages_history, address, seq_num, ip_orig, port_orig, key_info)

                current_address = servent_socket.getsockname()
                new_node =  ' ' + str(current_address[0]) + ':' + str(current_address[1])
                key_info = key_info + new_node

                #send response do client
                msg = utils.build_message_by_type(utils.RESP_TYPE, seq_num, key_info)
                utils.send_data_udp(servent_socket, ip_orig, port_orig, msg)

                spread_flood_message(address, message_type, ttl, seq_num, ip_orig, port_orig, key_info)


# terminates connection CTRL+C is typed
except KeyboardInterrupt as e:
    utils.close_connection(servent_socket)
    utils.debug('The servent socket connection is closed')

""" MAIN PROGRAM ENDS"""
