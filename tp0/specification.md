Last Updated on 15/08/17

Universidade Federal de Minas Gerais - DCC023: Redes de Computadores
Trabalho Prático 0

# Introduction

In this work we will develop a server that manages a global counter modified by several clients. In the text below, function names from the material documentation service documentation.

## Goals

* Enter the POSIX socket programming interface.
* Introduce the concepts of client application and server application.
* Introduce the concepts of coding and data transmission.

## Execution

* The work is worth 5 points.
* The delivery date is available in Moodle or Course Plan.

# Specification

## Client Program

The client program connects to the server [socket, bind, connect]. After establishing the connection, the client sends a (1) byte containing the "+" or "-" characters, requesting the increment or decrement of the global counter (module 1000, so that the counter can only assume values between 0 and 999) [Send]. After the request is sent, the client expects a 4-byte integer encoded in [network byte order](https://en.wikipedia.org/wiki/Endianness) from the server  [htonl, ntohl] containing the global counter value [recv]. After receiving the global counter, the client returns the received value to the server in three numeric characters (in ASCII format) [sprintf], first the character representing the hundreds, then tens and finally the units, confirming the receipt and processing of the message [ Send]. After the communication is sent, the client closes the connection to the [close] server.

## Server Program

The server program will initialize its counter with zero and will wait for client connection [socket, bind, listen, accept]. After establishing a connection, the server should receive one (1) request byte [recv]. Upon receipt of the request, the server shall process it and send the next global counter value to the [send] client; The counter value must be sent as an encoded 4-byte integer in [network byte order](https://en.wikipedia.org/wiki/Endianness) [htonl, ntohl]. After sending the next counter value, the server must wait three bytes containing the counter value confirmation by the [recv] client. Upon receipt of the confirmation (or timer trigger), the server should close the client connection [close]. If the value confirmed by the client is the next value of the [atoi] counter, the server updates the global counter and writes the new value to a new line in the standard output. If the server has timed out (1 second) it should write "T" on a new line. The server must then return to the initial stage to accept the next client connection. For ease of correction, your server should not write anything other than the one previously noted.

## Implementation Details

* The server should listen on IP address 127.0.0.1 (representing the local machine on which the program executes) and on port 51515.
* The '+' and '-' characters must be transmitted in 1 byte using ASCII encoding.
* The server must configure a timer (timeout) to detect communication failures when calling the [recv] function. The timer setting is done by calling the [setsockopt] function. On Linux, the options available for use in the [setsockopt] function are described in the [socket] section of manual 7 (access using [man 7 socket]). Search for [SO_RCVTIMEO]. In python the [settimeout] method is used and the exception handling [socket.timeout] is used.
* The server must print the new counter value in standard output (stdout) every time the counter is successfully modified.
* In Python, instead of using htonl / ntohl, you should use the [pack] and [unpack] functions.

# Delivery and Evaluation

You must deliver the source code for your program and a makefile with the "run" target, so your program will start when you run "make run", and an empty target to compile your program if you use C (that is, Execute "make" compiles and generates the executable). For this work you do not need to submit a report, but your code should be well organized and commented. Your program will be semi-automatically tested with the teacher reference implementation. To test the correct functioning of your server, test your program with the reference client available in moodle.



