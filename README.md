# dcc023_computer_networks

## Authors/Autores:  
https://github.com/ArturHFS  
https://gitlab.com/laura-vianna  

### Repository for group coursework of Computer Networks module - 2017/2  

1. TP0 - Global Counter, with Server and Client - Python3 e C  
2. TP1 - Data Framing - Python3  
3. TP2 - Event oriented messages system, using TCP protocol - Python3  
4. TP3 - Peer-to-peer aplication, with servents and clients, using UDP protocol - Python3  

### Repositorio para trabalho em grupo da disciplina Redes de Computadores - 2017/2  
1. TP0 - Contador global, com Servidor e Cliente - Python3 e C  
2. TP1 - Enquadramento de dados - Python3  
3. TP2 - Sistema de Mensagens Orientado a Eventos usando protocolo TCP - Python3  
4. TP3 - Aplicação peer-to-peer, com Servents e Clientes e protocolo UDP - Python3  
