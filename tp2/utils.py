# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP02 - Event oriented messages system

import sys
import socket
import struct

# TODO: turn off debugger
DEBUG = True

""" Messages Types """
OK_TYPE = 1
ERRO_TYPE = 2
OI_TYPE = 3
FLW_TYPE = 4
MSG_TYPE = 5
CREQ_TYPE = 6
CLIST_TYPE = 7

OIAP_TYPE = 13
MSGAP_TYPE = 15
CREQAP_TYPE = 16
CLISTAP_TYPE = 17
""" Messages Types """


# Prints debug messages if DEBUG is set
def debug(message):
    if DEBUG:
        print('[debug] ' + message)

def build_header(msg_type, sender_id, receiver_id, seq_num):
    debug("msg_type - sender_id - receiver_id - seq_num:")
    debug(str(msg_type) + " - " + str(sender_id) + " - " + str(receiver_id) + " - " + str(seq_num))
    return struct.pack("!H", msg_type) + struct.pack("!H", sender_id) + struct.pack("!H", receiver_id) + struct.pack("!H", seq_num)

def build_client_list_message(client_num, client_list):
    contents = struct.pack('!H', client_num)

    for client in client_list:
        contents += struct.pack('!H', client)

    return contents

def build_message_by_type(msg_type, sender_id, receiver_id, seq_num, contents = "", nickname = ""):
    if msg_type == OK_TYPE:
        msg = build_header(msg_type, sender_id, receiver_id, seq_num)
    elif msg_type == ERRO_TYPE:
        msg = build_header(msg_type, sender_id, receiver_id, seq_num)
    elif msg_type == OI_TYPE:
        msg = build_header(msg_type, 0, receiver_id, 0)
    elif msg_type == FLW_TYPE:
        msg = build_header(msg_type, sender_id, receiver_id, seq_num)
    elif msg_type == MSG_TYPE or msg_type == CLIST_TYPE or msg_type == OIAP_TYPE:
        ascii_contents = contents.encode('ascii')
        contents_length = len(ascii_contents)
        msg = build_header(msg_type, sender_id, receiver_id, seq_num) + struct.pack("!H", contents_length) + ascii_contents
    elif msg_type == MSGAP_TYPE:
        ascii_contents = contents.encode('ascii')
        contents_length = len(ascii_contents)
        ascii_nick = nickname.encode('ascii')
        nick_length = len(ascii_nick)
        msg = build_header(msg_type, sender_id, receiver_id, seq_num) + struct.pack("!H", nick_length) + ascii_nick + struct.pack("!H", contents_length) + ascii_contents
    elif msg_type == CREQ_TYPE:
        msg = build_header(msg_type, sender_id, receiver_id, seq_num)
    elif msg_type == CREQAP_TYPE:
        msg = build_header(msg_type, sender_id, receiver_id, seq_num)
    else:
        msg = build_header(ERRO_TYPE, sender_id, receiver_id, seq_num)
    return msg

# Initialize server connection on host:port, according to given mode
def initialize_connection(host, port, mode):
    current_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    if (mode == 'server'):
        current_socket.bind((host, port))
        current_socket.listen(1)
        return current_socket

    elif (mode == 'client'):
            current_socket.connect((host, port))
            return current_socket

def close_connection(socket):
    if socket is not None:
        socket.close()

# Sends 'data' through the 'current_socket' of 'size' bytes
# Raises an exception if the connection was lost
def send_data(current_socket, data, size):
    total_sent = 0

    # make sure all the data was sent, even if sent in different packages
    while(total_sent < size):
        sent = current_socket.send(data[total_sent:])
        if(sent == 0):
            raise RuntimeError('ERROR in send_data - Connection to client lost')

        total_sent += sent

# Receive 'data' through the 'current_socket' of 'size' bytes
# If the package needs to be reformatted, set 'to_unpack' and pass the right format string 'fmt'
# Raises an exception if the connection was lost
def receive_data(current_socket, size = 1, to_unpack = False, unpack_fmt = ''):
    total_received = 0
    data = bytearray()

    try:
        # make sure all the data was received, even if sent in different packages
        while(total_received < size):
            chunk = current_socket.recv(size - total_received)
            if(chunk == ''):
                raise RuntimeError('ERROR in receive_data - Connection to client lost')

            data += chunk
            total_received += len(chunk)

        # unpack data from network format to specified format
        if(to_unpack):
            data = struct.unpack(unpack_fmt, data)
            if(len(data) == 1):
                data = data[0]

        return data

    except socket.error as socketerror:
        current_socket.close()
        print("Error - socket connection is closed: ", socketerror)
        sys.exit(0)


# Reads receive and decode a message from the socket, as defined
# in the protocol
def receive_message(source_socket):
    message_type = receive_data(source_socket, 2, True, '!H')
    source_id = receive_data(source_socket, 2, True, '!H')
    dest_id = receive_data(source_socket, 2, True, '!H')
    sequence_number = receive_data(source_socket, 2, True, '!H')
    contents = ""

    if(message_type == MSG_TYPE or message_type == CLIST_TYPE or message_type == OIAP_TYPE):
        length = receive_data(source_socket, 2, True, '!H')
        content = receive_data(source_socket, length)
        contents = content.decode('ascii')

    elif(message_type == MSGAP_TYPE):
        nick_length = receive_data(source_socket, 2, True, '!H')
        nick = receive_data(source_socket, nick_length)

        length = receive_data(source_socket, 2, True, '!H')
        content = receive_data(source_socket, length)

        contents = [nick.decode('ascii'), content.decode('ascii')]

    # elif(message_type == CLIST_TYPE):
    #     length = receive_data(source_socket, 2, True, '!H')

    #     for i in xrange(length):
    #         cid = receive_data(source_socket, 2, True, '!H')
    #         contents += str(cid) + "\n"

    return int(message_type), int(source_id), int(dest_id), int(sequence_number), contents
