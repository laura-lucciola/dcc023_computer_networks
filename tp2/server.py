# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP02 - Event oriented messages system

import utils
import sys
import socket
import struct
import select


class Client:
    def __init__(self, csocket, address):
        self.socket = csocket
        self.address = address
        self.nickname = ""
        self.cid = -1


def close_connection(sockets, clients, source_id):
    sockets.remove(clients[source_id].socket)
    if(clients[source_id].nickname != ""):
        del nick_to_id[clients[source_id].nickname]

    del clients[source_id]
    used_id.remove(source_id)


try:
    if len(sys.argv) != 2:
        utils.debug('Wrong number of arguments! Must be "program <port>"')
        sys.exit(0)

    host = '127.0.0.1'
    port = int(sys.argv[1])
    server_id = 65535

    max_id = 65534
    used_id = []
    next_id = 1

    nick_to_id = {}

    msg_counter = 0

    con_socket = utils.initialize_connection(host, port, "server")
    sockets = [con_socket]

    temp_clients = []
    clients = {}

    while True:
        to_read, to_write, with_error = select.select(sockets, sockets, [])

        for s in to_read:
            if s is con_socket:
                c_socket, c_address = s.accept()
                c_socket.settimeout(5)

                utils.debug("Received connection")

                client = Client(c_socket, c_address)
                sockets.append(client.socket)
                temp_clients.append(client)

            else:
                message_type, source_id, dest_id, sequence_number, contents = utils.receive_message(s)
                utils.debug("Received message - " + str(message_type) + " - " + str(source_id) + " - " + str(dest_id) + " - " + str(sequence_number) + " - " + str(contents))

                # Verify if source_id corresponds to socket associated id
                if(message_type != utils.OI_TYPE and message_type != utils.OIAP_TYPE):
                    if(not (s is clients[source_id].socket)):
                        error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                        utils.send_data(s, error_message, len(error_message))

                        continue

                # OI message
                if(message_type == utils.OI_TYPE or message_type == utils.OIAP_TYPE):
                    # find which client sent OI message and remove it from temp list
                    for i, c in enumerate(temp_clients):
                        if s is c.socket:
                            client = temp_clients.pop(i)
                            break

                    # get next not used id
                    while next_id in used_id:
                        next_id += 1
                        if next_id > max_id:
                            next_id = 1

                    utils.debug("Received OI - source " + str(next_id))
                    client.cid = next_id
                    clients[client.cid] = client

                    used_id.append(next_id)
                    next_id += 1

                    # Associate client with nickname
                    if(message_type == utils.OIAP_TYPE):
                        if(contents not in nick_to_id):
                            client.nickname = contents
                            nick_to_id[client.nickname] = client.cid
                        else:
                            utils.debug("Sent Error")

                            error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                            utils.send_data(client.socket, error_message, len(error_message))
                            close_connection(sockets, clients, client.cid)

                            continue

                    ok_message = utils.build_message_by_type(utils.OK_TYPE, server_id, client.cid, sequence_number)
                    utils.send_data(client.socket, ok_message, len(ok_message))

                # FLW message
                elif(message_type == utils.FLW_TYPE):

                    utils.debug("Received FLW - source " + str(source_id))

                    # send ok message
                    ok_message = utils.build_message_by_type(utils.OK_TYPE, server_id, source_id, sequence_number)
                    utils.send_data(clients.get(source_id).socket, ok_message, len(ok_message))

                    utils.debug("Sent OK")

                    # remove references to closed client
                    close_connection(sockets, clients, source_id)


                # MSG message
                elif(message_type == utils.MSG_TYPE or message_type == utils.MSGAP_TYPE):

                    utils.debug("Received MSG - source " + str(source_id) + ", destination " + str(dest_id))

                    source = clients.get(source_id)

                    # get the destination id of corresponding nickname
                    if(message_type == utils.MSGAP_TYPE):
                        nick = contents[0]
                        contents = contents[1]

                        if(nick in nick_to_id):
                            dest_id = nick_to_id[nick]
                        else:
                            utils.debug("Sent Error")

                            error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                            utils.send_data(source.socket, error_message, len(error_message))

                            continue

                    # broadcast
                    if(dest_id == 0 and message_type == utils.MSG_TYPE):
                        for dest in clients.values():
                            if(dest.cid != source_id):
                                # foward message to destination
                                message = utils.build_message_by_type(utils.MSG_TYPE, source_id, dest_id, sequence_number, contents)
                                utils.send_data(dest.socket, message, len(message))

                                # receive ok message from destination
                                try:
                                    reply_type, reply_src_id, _, reply_seq_number, reply_data = utils.receive_message(dest.socket)
                                    utils.debug("Received OK from destination")


                                except socket.timeout as e:
                                    utils.debug("Destination timeout")
                                    # remove references to closed client
                                    close_connection(sockets, clients, dest.cid)

                        
                        utils.debug("Sent OK")

                        ok_message = utils.build_message_by_type(utils.OK_TYPE, server_id, source_id, sequence_number)
                        utils.send_data(source.socket, ok_message, len(ok_message))

                    # single destination
                    elif(dest_id in used_id):
                        dest = clients.get(dest_id)

                        # foward message to destination
                        message = utils.build_message_by_type(utils.MSG_TYPE, source_id, dest_id, sequence_number, contents)
                        utils.send_data(dest.socket, message, len(message))

                        # receive ok message from destination
                        try:
                            reply_type, reply_src_id, _, reply_seq_number, reply_data = utils.receive_message(dest.socket)
                            utils.debug("Received OK from destination")

                            if(reply_type == 1 and reply_seq_number == sequence_number):
                                utils.debug("Sent OK")

                                ok_message = utils.build_message_by_type(utils.OK_TYPE, server_id, source_id, sequence_number)
                                utils.send_data(source.socket, ok_message, len(ok_message))
                            else:
                                utils.debug("Sent Error")

                                error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                                utils.send_data(source.socket, error_message, len(error_message))

                        except socket.timeout as e:
                            utils.debug("Destination timeout")
                            # remove references to closed client
                            close_connection(sockets, clients, dest.cid)

                            error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                            utils.send_data(source.socket, error_message, len(error_message))

                    # invalid id
                    else:
                        error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                        utils.send_data(source.socket, error_message, len(error_message))


                # CREQ message
                elif(message_type == utils.CREQ_TYPE or message_type == utils.CREQAP_TYPE):
                    utils.debug("Received CREQ - source " + str(source_id))

                    id_list = ""
                    if(message_type == utils.CREQ_TYPE):
                        for i in used_id:
                            id_list += str(i) + "\n"
                    
                    else:
                        for key in nick_to_id.keys():
                            id_list += str(nick_to_id[key]) + " - " + key + "\n"

                    message = utils.build_message_by_type(utils.CLIST_TYPE, server_id, source_id, sequence_number, id_list)
                    utils.send_data(clients.get(source_id).socket, message, len(message))

                    utils.debug("Sent CLIST")

                    try:
                        # receive ok message from destination
                        reply_type, _, _, _, data = utils.receive_message(clients.get(source_id).socket)
                        utils.debug("Received OK from destination")

                        if(reply_type != utils.OK_TYPE):
                            error_message = utils.build_message_by_type(utils.ERRO_TYPE, server_id, source_id, sequence_number)
                            utils.send_data(clients.get(source_id).socket, error_message, len(error_message))


                    except socket.timeout as e:
                        # remove references to closed client
                        close_connection(sockets, clients, source_id)



# terminates connection CTRL+C is typed
except KeyboardInterrupt as e:
    current_socket.close()
    utils.debug('The server and client socket connection are closed')
