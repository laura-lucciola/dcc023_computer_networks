# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP02 - Event oriented messages system

import utils
import sys
import socket
import struct
import select

LOCAL_SEQ_NUM = 0
LOCAL_CLIENT_ID = -1
LOCAL_CLIENT_NICK = ""
SERVER_ID = 65535
FLW_STATE = False

CREQ_CMD = "/cl"
MSG_CMD = "/m"
CREQAP_CMD = "/clap"
MSGAP_CMD = "/map"
FLW_CMD = "/quit"

# The Client Socket will send an request message OIAP(13) or OI(3) to the server, requesting an
# indentifier for itself (LOCAL_CLIENT_ID) and it's starting sequence number (LOCAL_SEQ_NUM)
def receive_client_id(client_socket, msg_type, contents = ""):
    msg = utils.build_message_by_type(msg_type, 0, SERVER_ID, 0, contents)
    utils.send_data(client_socket, msg, len(msg))

    message_type, _, local_id, seq_num, _ = utils.receive_message(client_socket)
    if message_type != utils.OK_TYPE:
        utils.debug(str(msg_type) + ': returned an error message! Closing client socket connection')
        client_socket.close()
        sys.exit(0)
    else:
        print(str(msg_type) + ": The server has given you the ID: " + str(local_id))
        return local_id, seq_num

""" MAIN PROGRAM BEGINS"""

if len(sys.argv) < 3:
    utils.debug('Wrong number of arguments! Must include "python3 client.py <ip> <port>"')
    sys.exit(0)

host = sys.argv[1]
port = int(sys.argv[2])
if len(sys.argv) == 4:
    LOCAL_CLIENT_NICK = sys.argv[3]

client_socket = utils.initialize_connection(host, port, 'client')

print("Welcome to the CHAT (Channel of Hyper Amicable Talk)!")

if len(sys.argv) == 4:
    print("You have choosen the nickname: " + LOCAL_CLIENT_NICK)
    LOCAL_CLIENT_ID, LOCAL_SEQ_NUM = receive_client_id(client_socket, utils.OIAP_TYPE, LOCAL_CLIENT_NICK)
else:
    LOCAL_CLIENT_ID, LOCAL_SEQ_NUM = receive_client_id(client_socket, utils.OI_TYPE)

print("\nThis is how you use this program:")
print("To get a list with the IDs of all connected clients: " + str(CREQ_CMD))
print("To get a list with the nicknames of all connected clients: " + str(CREQAP_CMD))
print("To send a text message to another client: " + str(MSG_CMD) + " DESTINATION_ID message")
print("To send a text message to another client: " + str(MSGAP_CMD) + " DESTINATION_NICK message")
print("To close the connection in this client: " + str(FLW_CMD))


inputs = [client_socket, sys.stdin.fileno()]
while inputs:
    to_read, _, _ = select.select(inputs, inputs, [])
    for item in to_read:
        # When client is receiving a message from the socket
        if item is client_socket:
            message_type, source, dest_id, sequence_number, contents = utils.receive_message(item)
            dest_id = int(dest_id)
            sequence_number = int(sequence_number)

            if(message_type == utils.OK_TYPE):
                utils.debug('OK(1) message received')
                if FLW_STATE:
                    client_socket.close()
                    print("Received OK after FLW and closed this client connection")
                    sys.exit(0)

            elif (message_type == utils.ERRO_TYPE):
                utils.debug('ERRO(2) message received!')

            elif (message_type == utils.MSG_TYPE):
                print('Message from client with ID = '+ str(source) +':\n\t' + contents)
                msg = utils.build_message_by_type(utils.OK_TYPE, LOCAL_CLIENT_ID, SERVER_ID, sequence_number)
                utils.send_data(client_socket, msg, len(msg))

            elif (message_type == utils.CLIST_TYPE):
                print('Client list with IDs received:\n' + contents)
                msg = utils.build_message_by_type(utils.OK_TYPE, LOCAL_CLIENT_ID, SERVER_ID, sequence_number)
                utils.send_data(client_socket, msg, len(msg))

            elif (message_type == utils.MSGAP_TYPE):
                print('Message from client with nickname = '+ contents[0] +':\n\t' + contents[1])
                msg = utils.build_message_by_type(utils.OK_TYPE, LOCAL_CLIENT_ID, SERVER_ID, sequence_number)
                utils.send_data(client_socket, msg, len(msg))

            elif (message_type == utils.CLISTAP_TYPE):
                print('Client list with nicknames received:\n' + contents)
                msg = utils.build_message_by_type(utils.OK_TYPE, LOCAL_CLIENT_ID, SERVER_ID, sequence_number)
                utils.send_data(client_socket, msg, len(msg))

            else:
                print('Unknown type of message was received')
                pass

        # When client is reading user input from the command line
        else:
            read_input = input()
            split_input = read_input.split()
            command = str(split_input[0])
            if (command == CREQ_CMD):
                msg = utils.build_message_by_type(utils.CREQ_TYPE, LOCAL_CLIENT_ID, SERVER_ID, LOCAL_SEQ_NUM)
                utils.send_data(client_socket, msg, len(msg))

            elif (command == MSG_CMD):
                destination_id = int(split_input[1])
                contents = ' '.join(split_input[2:])
                msg = utils.build_message_by_type(utils.MSG_TYPE, LOCAL_CLIENT_ID, destination_id, LOCAL_SEQ_NUM, contents)
                utils.send_data(client_socket, msg, len(msg))

            elif (command == FLW_CMD):
                FLW_STATE = True
                msg = utils.build_message_by_type(utils.FLW_TYPE, LOCAL_CLIENT_ID, SERVER_ID, LOCAL_SEQ_NUM)
                utils.send_data(client_socket, msg, len(msg))

            elif (command == CREQAP_CMD):
                msg = utils.build_message_by_type(utils.CREQAP_TYPE, LOCAL_CLIENT_ID, SERVER_ID, LOCAL_SEQ_NUM)
                utils.send_data(client_socket, msg, len(msg))

            elif (command == MSGAP_CMD):
                destination_nick = split_input[1]
                contents = ' '.join(split_input[2:])
                msg = utils.build_message_by_type(utils.MSGAP_TYPE, LOCAL_CLIENT_ID, SERVER_ID, LOCAL_SEQ_NUM, contents, destination_nick)
                utils.send_data(client_socket, msg, len(msg))

            else:
                print('Command unknown! ' + command)
                pass

""" MAIN PROGRAM ENDS"""
